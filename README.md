# GitLab Runner template for Clever Cloud

## What?

This project is a starter kit to create and deploy easily a **[GitLab Runner](https://docs.gitlab.com/runner/)** on [Clever Cloud](https://www.clever-cloud.com/en/).

Since the moment you fork this project you can deploy the runner on **Clever Cloud**.

## Setup

- First, clone this project or fork this project (then go to the **General Settings** in the **Advanced** section and **Remove fork relationship**)
- Add these 7 environment variables to the CI/CD settings of your project (or group)
  - `CLEVER_SECRET`
  - `CLEVER_TOKEN`
  - `ORGANIZATION_ID`
  - `ORGANIZATION_NAME` *(optional)*
  - `GITLAB_ACCESS_TOKEN` *[token documentation](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)*
  - `GITLAB_INSTANCE` *e.g.: [https://gitlab.com](https://gitlab.com)*
  - `CI_REGISTRATION_TOKEN` *see the CI/CD settings of your project or group or admin pannel in the `Runners` section to get the token*

> - To get `CLEVER_SECRET` and `CLEVER_TOKEN`, you need to install the [Clever Tools CLI](https://github.com/CleverCloud/clever-tools) and then you have to [login](https://github.com/CleverCloud/clever-tools#how-to-use) with this command `clever login`. It will open a page in your browser. Copy the provided token and secret in the **Variables** section of the CI/CD settings
> - You can find the `ORGANIZATION_ID` in the Clever Cloud web administration console ([https://console.clever-cloud.com/](https://console.clever-cloud.com/)).

> - :wave: :warning: don't forget, when using this runner, to use its tag in the `.gitlab-ci.yml` (with the yaml key `tags:`)
> - *the tag name of the runner is defined in the `go.sh` file (see the **FAQ** section)*

## Update the `.gitlab-ci.yml`

Change the value of the `CLEVER_APPLICATION_NAME` variable with the name of the runner

```yaml
variables:
  # Runner name
  CLEVER_APPLICATION_NAME: "runner-cc-demo"
```

## FAQ

> **How to change the tag name of the runner?**
> - Edit the `go.sh` file and change the value of the registration option `--tag-list "cc-runner"`

> **How to add dependencies, applications, etc. ... to my runner?**
> - Edit the `Dockerfile` file (e.g.: in this template, we installed nodjs and openjdk)

## Thanks

- :sparkles: :sparkles: :sparkles: to [@hsablonniere](https://gitlab.com/hsablonniere) for his :construction: on the `.gitlab-ci.yml` file

## Disclaimer :wave:

We provide this tools for educational purposes. It is subject to an opensource license. You can modify them according to your needs. These projects are not part of the Clever Cloud support. However, if you need help do not hesitate to create an issue in the associated project: **[Create an issue](https://gitlab.com/CleverCloud/tools/gl-runner-template/issues)**.


